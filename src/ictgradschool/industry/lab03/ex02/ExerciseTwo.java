package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        System.out.print("Lower bound? ");
        int x = Integer.parseInt(Keyboard.readInput().trim());
        System.out.print("Upper bound?");
        int y = Integer.parseInt(Keyboard.readInput().trim());
        int a = x + (int)((y - x)*Math.random());
        int b = x + (int)((y - x)*Math.random());
        int c = x + (int)((y - x)*Math.random());
        int d = Math.min(Math.min(a, b), Math.min(b, c));
        System.out.println("Of the numbers " + a + " " + b + " " +c + ", the smallest value is " + d);

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
